provider "aws" {
  region  = "us-east-1"
  profile = "awsci"
}

module "vpc_standard" {
  source     = "../../"
  cidr_block = "10.0.0.0/20"
}
